window.addEventListener('DOMContentLoaded', () => {

  // create menu copy
  const navbar = document.querySelector('.navbar-nav')
  const modal = document.querySelector('#menu_modal .modal-body')
  const navClone = navbar.cloneNode(true)
  modal.appendChild(navClone)

  // select 
  const selectElm = document.querySelectorAll('.js-choice');
  selectElm.forEach(e => {
    new Choices(e, {
      itemSelectText: 'اختر',
      removeItemButton: true,
    });
  })

  // some jQuery 
  if (document.querySelectorAll('.file_input')) {
    $(".file_input").fileinput({
      enableResumableUpload: true,
      uploadUrl: "/site/test-upload",
      language: 'ar',
      theme: 'fas',
      rtl: true,
      showUpload: false,
    });
  }

  //nice number
  $('.input_nice_number').length > 0 ? $('.input_nice_number').niceNumber() : null
})